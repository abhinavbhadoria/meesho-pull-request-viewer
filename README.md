# Git+: Github Pull Request Viewer (Meesho Assignment)

### Important: Build the presentation module to generate the application. Apk is also provided in the root of the project.

## Architecture

The architecture of the project follows the principles of Clean Architecture.

Source: https://github.com/android10/Android-CleanArchitecture

The application is divided into 3 modules, each following a clean dependency on layers prior to it. The layers are as:

### Domain

The domain layer consists of Reactive UseCases that performs a particular action such as Get Pull Request for given input and returns an Observable/Completable/Single that is used in later layers to listen to the changes. It also defines the set of operation it expects Data layer to perform through respective Repository interfaces (such as IGithubRepository).

### Data

The data layer implements Repository design pattern and wraps both remote and local storage. GithubRemoteRepository wraps the Retrofit client whereas GithubLocalRepository wraps the Room database. GithubRepositoryFactory provides the appropriate repository based on current network state.

### Presentation

The presentation layers host all the ViewModels, State and Activities. Databinding is used to fill UI elements. ListAdapter is used for efficient list addition/updating.


## Important Classes and Things

- Both request and response are cached offline using Room and SharedPref.

- Splash activity has no layout file, instead the ui is populated using theme for faster load.

- GithubRepository factory gets injected ConnectivityManager that tells the current network state for smooth user experience.

- Project contains a option button that links to my personal applications on Google Play (http://smarturl.it/zerologicgames)

![Splash Screen]
(https://bitbucket.org/abhinavbhadoria/meesho-pull-request-viewer/raw/7c9b522927c475f01f04494222deecdf3f15c528/screenshots/splash.png)

![Repo Detail Request]
(https://bitbucket.org/abhinavbhadoria/meesho-pull-request-viewer/raw/327458fc7821ed781fb9de38abf082c34e397c97/screenshots/repo_detail_request.png)

![Online Results]
(https://bitbucket.org/abhinavbhadoria/meesho-pull-request-viewer/raw/327458fc7821ed781fb9de38abf082c34e397c97/screenshots/online_results.png)

![Cached Results]
(https://bitbucket.org/abhinavbhadoria/meesho-pull-request-viewer/raw/327458fc7821ed781fb9de38abf082c34e397c97/screenshots/cached_results.png)
